import { RedContainer } from './routes/dashboard/components/red/red.container';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';
import { SigninOidcComponent } from './signin-oidc/signin-oidc.component';
import { RequireAuthenticatedUserRouteGuardService } from './shared/services/require-authenticated-user-route-guard.service';
import { OpenIdConnectService } from './shared/services/open-id-connect.service';
import { YellowContainer } from './routes/dashboard/components/yellow/yellow.container';
import { AquaContainer } from './routes/dashboard/components/aqua/aqua.container';
import { MagentaContainer } from './routes/dashboard/components/magenta/magenta.container';
import { OrangeContainer } from './routes/dashboard/components/orange/orange.container';
import { EnsureAcceptHeaderInterceptor } from './shared/interceptors/ensure-accept-header-interceptor';
import { WriteOutJsonInterceptor } from './shared/interceptors/write-out-json-interceptor';
import { HandleHttpErrorInterceptor } from './shared/interceptors/handle-http-error-interceptor';
import { AddAuthorizationHeaderInterceptorService } from './shared/interceptors/add-authorization-header-interceptor.service';
import { RedirectSilentRenewComponent } from './redirect-silent-renew/redirect-silent-renew.component';

// https://github.com/ocombe/ng2-translate/issues/218
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        SigninOidcComponent,
        RedirectSilentRenewComponent
    ],
    imports: [
        HttpClientModule,
        BrowserAnimationsModule, // required for ng2-tag-input
        CoreModule,
        LayoutModule,
        SharedModule.forRoot(),
        RoutesModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    providers: [  
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AddAuthorizationHeaderInterceptorService,
            multi: true,
        },         
        RequireAuthenticatedUserRouteGuardService,
        OpenIdConnectService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
