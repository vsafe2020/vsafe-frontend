import { Routes } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';
import { SigninOidcComponent } from '../signin-oidc/signin-oidc.component';
import { RequireAuthenticatedUserRouteGuardService } from '../shared/services/require-authenticated-user-route-guard.service';
import { RedirectSilentRenewComponent } from '../redirect-silent-renew/redirect-silent-renew.component';

export const routes: Routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            // {
            //     path: 'home',
            //     canActivate: [RequireAuthenticatedUserRouteGuardService],
            //     loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
            // },           
            {
                path: 'home',
                 canActivate: [RequireAuthenticatedUserRouteGuardService],
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'signin-oidc',
                component: SigninOidcComponent
            },
            {
                path: 'redirect-silentrenew',
                component: RedirectSilentRenewComponent
            },
            {
                path: 'employee',
                 canActivate: [RequireAuthenticatedUserRouteGuardService],
                loadChildren: () => import('./employee/employee.module').then(m => m.EmployeeModule)
            },
            {
                path: 'department',
                 canActivate: [RequireAuthenticatedUserRouteGuardService],
                loadChildren: () => import('./department/department.module').then(m => m.DepartmentModule)
            }

        ]
    },

    // Not found
    { path: '**', redirectTo: 'home' }

];
