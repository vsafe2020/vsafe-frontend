export class User {
    id: string;
    name: string
    userName: string;
    fName:string;    
    lName:string;
    email:string;
    role:string;
    employeeNumber : string;    
}