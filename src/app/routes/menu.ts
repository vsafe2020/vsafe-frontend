
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};
const Employee = {
    text: 'Employee',
    link: '/employee',
    icon: "icon-user"
};

const Department = {
    text: 'Department',
    link: '/department',
    icon: "icon-people"
};

// const Settings = {
//     text: "Settings",    
//     icon: "icon-settings",
//     submenu: [
//         {
//             text: 'User',
//             icon: "icon-user",
//             link: '/user'
//         }        
//     ]
// };

const headingMain = {
    text: 'Main Navigation',
    heading: true
};

export const menu = [
    headingMain,
    Home,
    Employee,
    Department
];
