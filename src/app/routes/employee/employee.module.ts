import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxSelectModule } from 'ngx-select-ex';

const routes: Routes = [
  { path: '', component: EmployeeListComponent },
  { path: 'createemployee', component: CreateEmployeeComponent }
];

@NgModule({
  declarations: [EmployeeListComponent, CreateEmployeeComponent],
  imports: [
    CommonModule,
    SharedModule,
    Ng2SmartTableModule,
    NgxSelectModule,
    RouterModule.forChild(routes),
  ]
})
export class EmployeeModule { }
