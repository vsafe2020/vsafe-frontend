import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee.model';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  public employeeList: Employee[];    
  tableData: Employee[];    
  settings = {

    columns: {
      id: {
        title: 'Employee Id'
      },
      name: {
        title: 'Name'
      }
    },
    actions:{add: false, edit:false, delete:true},
  }; 

  constructor() { }

  ngOnInit(): void {

    this.tableData = [ { id : "123",name : "dept 1" },
    { id : "123",name : "dept 1"},
    { id : "124",name : "dept 2" },
    { id : "125",name : "dept 3"},
    { id : "126",name : "dept 4" },
    { id : "127",name : "dept 5" } ]
  }

}
