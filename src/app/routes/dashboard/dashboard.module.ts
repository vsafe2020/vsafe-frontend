import { YellowContainer } from './components/yellow/yellow.container';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { RedComponent } from './components/red/red.component';
import { YellowComponent } from './components/yellow/yellow.component';
import { AquaComponent } from './components/aqua/aqua.component';
import { MagentaComponent } from './components/magenta/magenta.component';
import { OrangeComponent } from './components/orange/orange.component';
import { DashboardCardContainer } from './dashboard/dashboard-card.container';
import { RedContainer } from './components/red/red.container';
import { AquaContainer } from './components/aqua/aqua.container';
import { MagentaContainer } from './components/magenta/magenta.container';
import { OrangeContainer } from './components/orange/orange.container';
import { GridsterModule } from 'angular-gridster2';
import { GridComponent } from './grid-components/grid/grid.component';
import { ParentContainerComponent } from './grid-components/parent-container/parent-container.component';
import { WidgetComponent } from './grid-components/widget/widget.component';
import { SummaryCardsComponent } from './grid-components/widget-component/summary-cards/summary-cards.component';
import { LeadsOverviewComponent } from './grid-components/widget-component/leads-overview/leads-overview.component';
import { CalendarComponent } from './grid-components/widget-component/calendar/calendar.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
];

@NgModule({
  declarations: [
    DashboardComponent,
    RedComponent,
    YellowComponent,
    AquaComponent,
    MagentaComponent,
    OrangeComponent,
    DashboardCardContainer,
    RedContainer,
    YellowContainer,
    AquaContainer,
    MagentaContainer,
    OrangeContainer,
    GridComponent,
    ParentContainerComponent,
    WidgetComponent,
    SummaryCardsComponent,
    LeadsOverviewComponent,
    CalendarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    GridsterModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    RedComponent,
    YellowComponent,
    AquaComponent,
    MagentaComponent,
    OrangeComponent,
  ],
  exports: [
    RouterModule,
    DashboardComponent,
    RedComponent,
    YellowComponent,
    AquaComponent,
    MagentaComponent,
    OrangeComponent,
]
})
export class DashboardModule { }
