import { Component } from '@angular/core';
import { DashboardCardContainer } from '../../dashboard/dashboard-card.container';



@Component({
  template: `<app-aqua></app-aqua>`,
})
export class AquaContainer extends DashboardCardContainer {

  constructor() {
    super();
  }
}
