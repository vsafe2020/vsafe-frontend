import { RedContainer } from './../components/red/red.container';
import { YellowContainer } from '../components/yellow/yellow.container';
import { AquaContainer } from '../components/aqua/aqua.container';
import { MagentaContainer } from '../components/magenta/magenta.container';
import { OrangeContainer } from '../components/orange/orange.container';


export const DashboardCards = {
  RED: RedContainer,
  YELLOW: YellowContainer,
  AQUA: AquaContainer,
  MAGENTA: MagentaContainer,
  ORANGE: OrangeContainer,
};
