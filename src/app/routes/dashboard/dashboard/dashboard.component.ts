import { DashboardOption } from './../../../shared/model/dashboard/dashboard-option.model';
import { Component, OnInit, ViewChildren, QueryList, ComponentFactoryResolver, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { DashboardOutletDirective } from 'src/app/shared/directives/dashboard-outlet.directive';
import { DashboardCardContainer } from './dashboard-card.container';
import { DashboardCards } from './dashboard.card';
import { RedComponent } from '../components/red/red.component';
import { YellowComponent } from '../components/yellow/yellow.component';
import { AquaComponent } from '../components/aqua/aqua.component';
import { MagentaComponent } from '../components/magenta/magenta.component';
import { OrangeComponent } from '../components/orange/orange.component';
import { GridsterConfig, GridsterItem }  from 'angular-gridster2';
import { trigger, transition, style, animate } from '@angular/animations';
import { Subject } from 'rxjs';
import { OpenIdConnectService } from 'src/app/shared/services/open-id-connect.service';
import { User } from 'oidc-client';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('200ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ]),
    trigger('fade', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate(3000, style({opacity: 1}))
      ])
    ])
  ]
})
export class DashboardComponent implements OnInit {

  loggedInUser: User;
  dashboardOPtions: DashboardOption[];
  isDashboardOPtionsPanel = false;
  dashboardOptionEventSubject: Subject<DashboardOption[]> = new Subject<DashboardOption[]>();
  constructor(private openIdConnectService: OpenIdConnectService) { }

  ngOnInit(): void {
    this.getLoggedInUser();
    this.initDashboardOptions();
  }

  initDashboardOptions() {
    this.dashboardOPtions = [
      { Name: 'Summary Cards', Type: 'summary-cards', Rows: 1, Cols: 4, IsChecked: true },
      { Name: 'Calendar', Type: 'calender', Rows: 4, Cols: 4, IsChecked: true },
      { Name: 'Kanban Board', Type: 'kanban', Rows: 4, Cols: 4, IsChecked: true },
      { Name: 'Leads Overview Chart', Type: 'leads-overview', Rows: 3, Cols: 2, IsChecked: true },
      { Name: 'To Do List', Type: 'todolist', Rows: 3, Cols: 2, IsChecked: true },
    ];
  }

  dashboardOptionChanged() {
    this.dashboardOptionEventSubject.next(this.dashboardOPtions);
  }

  toggleDashboardOptionPanel() {
    this.isDashboardOPtionsPanel = !this.isDashboardOPtionsPanel;
  }

  private getLoggedInUser() {
    this.loggedInUser = this.openIdConnectService.user;
  }
}
