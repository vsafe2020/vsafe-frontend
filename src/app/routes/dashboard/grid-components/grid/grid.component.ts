import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnDestroy
} from '@angular/core';

import {
  CompactType,
  DisplayGrid,
  GridsterConfig,
  GridsterItem,
  GridType
} from 'angular-gridster2';
import { DashboardOption } from 'src/app/shared/model/dashboard/dashboard-option.model';
import { Observable, Subscription } from 'rxjs';


@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: [ './grid.component.scss' ]
})
export class GridComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  options: GridsterConfig;
  dashboard: Array<GridsterItem>;

  @Input() dashboardOptionEvent: Observable<DashboardOption[]>;

  itemChange(item, itemComponent) {
    console.info('itemChanged', item, itemComponent);
  }

  constructor() { }

  ngOnInit() {
    this.options = {
      itemChangeCallback: this.itemChange,
      gridType: GridType.Fit,
      compactType: CompactType.CompactLeftAndUp,
      margin: 10,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: true,
      mobileBreakpoint: 200,
      minCols: 1,
      maxCols: 4,
      minRows: 1,
      maxRows: 100,
      maxItemCols: 4,
      minItemCols: 1,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemArea: 2500,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 105,
      fixedRowHeight: 105,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 50,
      ignoreMarginInRow: false,
      setGridSize: true,
      draggable: {
        enabled: true,
      },
      resizable: {
        enabled: true,
      },
      swap: true,
      pushItems: false,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: {north: true, east: true, south: true, west: true},
      pushResizeItems: false,
      displayGrid: DisplayGrid.None,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false
    };

    this.dashboard = [
      {cols: 4, rows: 1, y: 0, x: 0, resizeEnabled: false, type: 'summary-cards'},
      {cols: 4, rows: 4, y: 0, x: 0, resizeEnabled: false, type: 'calender'},
      {cols: 4, rows: 4, y: 0, x: 0, resizeEnabled: false, type: 'kanban'},
      {cols: 2, rows: 3, y: 0, x: 0, resizeEnabled: false, type: 'leads-overview'},
      {cols: 2, rows: 3, y: 0, x: 0, resizeEnabled: false, type: 'todolist'}
    ];

    this.subscription.add(this.dashboardOptionEvent.subscribe(res => {
      const offWidgets = res.filter(x => x.IsChecked === false);
      if (offWidgets?.length > 0) {
        offWidgets.forEach(elem => {
          const dashboardItem = this.dashboard.filter(x => x.type === elem.Type);
          if (dashboardItem?.length > 0) {
            this.dashboard.splice(this.dashboard.indexOf(dashboardItem[0]), 1);
          }
        });
      }

      const onWidgets = res.filter(x => x.IsChecked === true);
      if (onWidgets?.length > 0) {
        onWidgets.forEach(elem => {
          const dashboardItem = this.dashboard.filter(x => x.type === elem.Type);
          if (!dashboardItem || dashboardItem.length === 0) {
            const widget = { cols: elem.Cols, rows: elem.Rows, y: 0, x: 0, resizeEnabled: false, type: elem.Type };
            this.dashboard.push(widget);
          }
        });
      }
      setTimeout(() => {
        this.options.api.optionsChanged();
      }, 1000);
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
