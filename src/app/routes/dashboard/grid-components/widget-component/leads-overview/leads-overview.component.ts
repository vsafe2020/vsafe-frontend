import { Component, OnInit } from '@angular/core';
import { Label, MultiDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
  selector: 'app-leads-overview',
  templateUrl: './leads-overview.component.html',
  styleUrls: ['./leads-overview.component.scss']
})
export class LeadsOverviewComponent implements OnInit {

   // Doughnut
  public doughnutChartLabels: Label[] = [
    'New', 'Contacted', 'Qualified',
  'Working', 'Proposal Sent', 'Customer', 'Lost Leads'];
  public doughnutChartData: MultiDataSet = [
    [10, 8, 5, 5, 4, 3, 2]
  ];
  public doughnutChartType: ChartType = 'doughnut';
  
  constructor() { }

  ngOnInit(): void {
  }

   // events
   public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
