import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-parent-container',
  templateUrl: './parent-container.component.html',
  styleUrls: ['./parent-container.component.scss']
})
export class ParentContainerComponent implements OnInit {

  @Input()
  widget;
  
  constructor() { }

  ngOnInit(): void {
  }

}
