import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { GenericValidatorService } from 'src/app/shared/services/generic-validator.service';
import { Employee } from '../../employee/model/employee.model';
import { EmployeeService } from '../../employee/service/employee.service';
import { Department } from '../model/department.model';
import { DepartmentService } from '../service/department.service';

@Component({
  selector: 'app-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.scss']
})
export class EditDepartmentComponent implements OnInit, OnDestroy {

  department: Department;

  employees: Employee[];

  //Represents the blufi form.
  departmentForm: FormGroup;

  //Performs the validation for the form.
  private genericValidator: GenericValidatorService;

  //Represents the validation error messages for controls
  displayMessage: { [key: string]: string } = {};

  id: string;
  sub: Subscription;
  public items: Array<string> = ['Jignesh Mistry', 'Shailesh Mistry', 'Amar Nagarsurkar',
    'Ketan Mistry', 'Rahi Bhagat'];
  selectedItem = 'Jignesh Mistry';

  //#region CONSTRUCTOR

  constructor(public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private employeeService: EmployeeService,
    private toastr: ToastrService,
    private departmentService: DepartmentService) {
  }

  //#endregion

  //#region  IMPLEMENTED METHODS

  ngOnInit(): void {

    //Get the employees from DB.
    this.getEmployees();

    this.initializeDepartmentForm();

    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getDepartmentDetails(this.id);
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  //#endregion

  //#region CLICK HANDLER

  //Represents the click handler for the Save department button.
  public departmentFormSubmit() {

    if (this.departmentForm.valid) {

      this.department.name = this.departmentForm.value.name;
      this.department.departmenthead = this.departmentForm.value.departmentHead;
      if (this.department.id === "00000000-0000-0000-0000-000000000000") {
        this.createDepartment();
      }
      else {
        this.departmentService.updateDepartment(this.department.id, this.department)
          .subscribe((data) => {
            this.toastr.success("Department saved successfully!");
            this.router.navigate(['/department']);
          });
      }
    }
    else {
      // Do nothing..
    }
  }

  //#endregion

  //#region PRIVATE METHODS

  //Initialize the Blufi Form.
  private initializeDepartmentForm() {

    //Init department form
    this.departmentForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      id: ['', Validators.required],
      departmentHead: ['ba37daa9-8234-4683-0cfd-08d875034eb1']
    });

    // Define an instance of the validator for use with this form, 
    // passing in this form's set of validation messages.
    this.genericValidator = new GenericValidatorService(
      this.translateService.instant('department.EDITDEPARTMENTFORM.VALIDATIONERRORS'));

    this.subscribeToFormValueChanges();
  }

  private displayDepartment() {

    //this.departmentForm.reset();

    this.departmentForm.patchValue({
      name: this.department.name,
      id: this.department.id,
      departmentHead: this.department.departmenthead
    });
  }

  private getDepartmentDetails(id) {

    if (id === "00000000-0000-0000-0000-000000000000") {
      this.department = new Department();
      this.displayDepartment();
    }
    else {
      this.departmentService.getDepartmentDetails(id).subscribe((data: Department) => {
        this.department = data;
        this.displayDepartment();
      });
    }
  }

  private createDepartment() {

    this.departmentService.createDepartment(this.department)
      .subscribe((data) => {
        this.toastr.success("Department created successfully!");
        this.router.navigate(['/department']);
      });
  }

  private subscribeToFormValueChanges(): void {

    //subscrie to the control value changes observable and
    //perform validation.
    this.departmentForm.valueChanges.pipe(
      debounceTime(800)
    ).subscribe(value => {
      this.displayMessage = this.genericValidator.processMessages(this.departmentForm);
    });
  }

  private getEmployees() {
    this.employees = 
    [
      {"id":"ba37daa9-8234-4683-0cfd-08d875034eb1","name":"jkm"},
      {"id":"9d329763-fa0a-4682-59f1-08d87502f42e","name":"Loc 1"}
    ]

    // this.employeeService.getEmployees().subscribe(data => {
    //   this.employees = data;
    // });
  }

  //#endregion


}
