import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Department } from '../model/department.model';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  Url = environment.apiUrl;
  
  constructor(private http: HttpClient) { 

  }

  getDepartments(): Observable<Department[]> {   
   
    return this.http.get<Department[]>(this.Url + 'departments?Fields=Id,Name');
  }

  getDepartmentDetails(Id): Observable<Department> {   
   
    return this.http.get<Department>(this.Url +`departments/${Id}?Fields=Id,Name,DepartmentHead`);
  }

  updateDepartment(Id, department: Department) {
    return this.http.put<Number>(this.Url + `departments/${Id}`, department);
  }

  deleteDepartment(Id){
    return this.http.delete<Number>(this.Url + `departments/${Id}`);
  }

  createDepartment(department: Department){
    return this.http.post<String>(this.Url + `departments`,department);
  }
}
