import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DepartmentlistComponent } from './departmentlist/departmentlist.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CreateDepartmentComponent } from './create-department/create-department.component';
import { EditDepartmentComponent } from './edit-department/edit-department.component';
import { NgxSelectModule } from 'ngx-select-ex'

const routes: Routes = [
  { path: '', component: DepartmentlistComponent },
  { path: 'createdepartment', component: CreateDepartmentComponent },
  { path: 'editdepartment/:id', component: EditDepartmentComponent }
];

@NgModule({
  declarations: [DepartmentlistComponent, CreateDepartmentComponent, EditDepartmentComponent],
  imports: [
    SharedModule,
    CommonModule,
    Ng2SmartTableModule,
    NgxSelectModule,
    RouterModule.forChild(routes),
  ]
})
export class DepartmentModule { }
