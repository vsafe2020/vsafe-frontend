//#region IMPORT

import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Department } from '../model/department.model';
import { DepartmentService } from '../service/department.service';

//#endregion

@Component({
  selector: 'app-departmentlist',
  templateUrl: './departmentlist.component.html',
  styleUrls: ['./departmentlist.component.scss']
})
export class DepartmentlistComponent implements OnInit {

  //#region PUBLIC MEMBER

  //Represents the datasource for a table
  public departmentList: Department[];

  //Represnts the column definitions.
  public departmentColumns: Array<any> = [];

  //Represents the reference to the action column template.
  @ViewChild('actionsColTmpl', { static: true }) actionsColTmpl: TemplateRef<any>;

  //#endregion

  //#region CONSTRUCTOR

  constructor(private router: Router,
    private toastr: ToastrService,
    private translateService: TranslateService,
    private departmentService: DepartmentService) { }

  //#endregion

  //#region IMPLEMENTATION METHOD

  ngOnInit(): void {

    //configure the datatable.
    this.prepareDepartmentColumns();

    //get the department data from DB.
    this.getDepartmentList();

  }

  //#endregion

  //#region CLICK HANDLER

  //click event handler for edit department button.
  public editDepartmentClicked(department: Department) {

    this.router.navigate(['/department/editdepartment/' + department.id]);
  }

  //click event handler for delete department button.
  public deleteDepartmentClicked(department: Department) {

    this.departmentService.deleteDepartment(department.id).subscribe(data => {

      this.toastr.success("Department deleted successfully!");
      this.getDepartmentList();
    });
  }

  //#endregion

  //#region PRIVATE METHODS

  private prepareDepartmentColumns() {
    
    this.departmentColumns.push({ prop: 'id', name: this.translateService.instant('department.list.id'), flexGrow: 3 });
    this.departmentColumns.push({ prop: 'name', name: this.translateService.instant('department.list.name'), flexGrow: 3 });
    this.departmentColumns.push({ prop: 'departmenthead', name: this.translateService.instant('department.list.departmenthead'), flexGrow: 3 });
    this.departmentColumns.push({ name:this.translateService.instant('department.list.action'), cellTemplate: this.actionsColTmpl, sortable: false, flexGrow: 3 });
  }

  private getDepartmentList() {

    this.departmentService.getDepartments().subscribe(data => {

      this.departmentList = data;

    });
  }

   //#endregion

}
