import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-data-table',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  @Input() rows;
  @Input() columns;
  @Input() limit;
  @Input() reorderable;
  @Input() sortColumn;
  @Input() sortOrder;
  @Input() selected = [];
  @Input() selectionType = undefined;
  @Input() selectAllRowsOnPage = false;
  @Output() select = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(event) {
    this.select.emit(event);
  }

}
