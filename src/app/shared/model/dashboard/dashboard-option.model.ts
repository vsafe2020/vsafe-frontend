export interface DashboardOption {
    Name: string;
    Type: string;
    Rows: number;
    Cols: number;
    IsChecked: boolean;
}
