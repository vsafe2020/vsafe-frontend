export class KanbanBoard {
    constructor(public name: string, public columns: Column[]) { }
}
export class Column {
    constructor(public name: string, public tasks: Lead[]) { }
}
  
export class Lead {
constructor(public header: string, public leadSource: string, public createdDate: string) { }
}
