import { Component, OnInit, OnDestroy } from '@angular/core';

import { UserblockService } from './userblock.service';
import { User } from 'oidc-client';
import { OpenIdConnectService } from 'src/app/shared/services/open-id-connect.service';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit, OnDestroy {
    user: any;
    loggedInUser: User;
    private subscription: Subscription = new Subscription();
    constructor(public userblockService: UserblockService,
                public openIdConnectService: OpenIdConnectService) {

        this.user = {
            picture: 'assets/img/user/01.jpg'
        };
    }


    ngOnInit() {

    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}
