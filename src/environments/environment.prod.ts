export const environment = {
  production: true,
  apiUrl: 'https://libertybackend.azurewebsites.net/api/',
  openIdConnectSettings: {
    authority: 'https://libertyidentity.azurewebsites.net',
    client_id: 'tourmanagementclient',
    redirect_uri: 'https://libertycrm.azurewebsites.net/signin-oidc',
    scope: 'openid profile roles tourmanagementapi',
    response_type: 'id_token token',
    post_logout_redirect_uri: 'https://libertycrm.azurewebsites.net/',
    // automaticSilentRenew: true,
    // silent_redirect_uri: 'https://libertycrm.azurewebsites.net/redirect-silentrenew',
    // loadUserInfo: true
  }
};

