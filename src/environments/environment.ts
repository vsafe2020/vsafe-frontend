// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://localhost:44308/api/',
  openIdConnectSettings: {
    authority: 'https://localhost:44361/',
    client_id: 'vsafewebclient',
    redirect_uri: 'https://localhost:4200/signin-oidc',
    scope: 'openid profile roles vsafeapi',
    response_type: 'id_token token',
    post_logout_redirect_uri: 'https://localhost:4200/',
    automaticSilentRenew: true,
    silent_redirect_uri: 'https://localhost:4200/redirect-silentrenew',
    loadUserInfo: true
  },
};

// export const environment = {
//   production: true,
//   apiUrl: 'https://libertybackend.azurewebsites.net/api/',
//   openIdConnectSettings: {
//     authority: 'https://libertyidentity.azurewebsites.net/',
//     client_id: 'tourmanagementclient',
//     redirect_uri: 'https://libertycrm.azurewebsites.net/signin-oidc',
//     scope: 'openid profile roles tourmanagementapi',
//     response_type: 'id_token token',
//     post_logout_redirect_uri: 'https://libertycrm.azurewebsites.net/',
//     // automaticSilentRenew: false,
//     // silent_redirect_uri: 'https://libertycrm.azurewebsites.net/redirect-silentrenew',
//     // loadUserInfo: true
//   }
// };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
